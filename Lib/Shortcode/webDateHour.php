<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use Datetime;
use DateTimeZone;
use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webDateHour
 * Displays the date and / or time, depending on the time zone and the desired format.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webDateHour extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webDateHour(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $datehour = new DateTime();

            if (isset($params['zone'])) {
                $datehour->setTimezone(new DateTimeZone($params['zone']));
            }              
            
            $content = str_replace($shorts[0][$x], $datehour->format($params['format']), $content);
        }

        return $content;
    }
}