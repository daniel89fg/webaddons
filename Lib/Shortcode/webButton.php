<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webButton
 * Design and display a button.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webButton extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        
        $shorts = static::searchCode($content, "/\[webButton(.*?)\][\r\n|\n]*(.*?)[\r\n|\n]*\[\/webButton\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }

        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $class = isset($params['class']) ? $params['class'] : '';
            $id = isset($params['id']) ? $params['id'] : '';
            $onclick = isset($params['onclick']) ? $params['onclick'] : '';

            $html = '<button class="'.$class.'" id="'.$id.'" onclick="'.$onclick.'">'.$shorts[2][$x].'</button>';

            $content = str_replace($shorts[0][$x], $html, $content);
        }

        return $content;
    }
}