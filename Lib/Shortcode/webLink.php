<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webLink
 * Create a hyperlink with its properties
 *
 * @author Athos Online <info@athosonline.com>
 */
class webLink extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        
        $shorts = static::searchCode($content, "/\[webLink(.*?)\][\r\n|\n]*(.*?)[\r\n|\n]*\[\/webLink\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        $appSettings = static::toolBox()->appSettings();
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $class = isset($params['class']) ? $params['class'] : '';
            $id = isset($params['id']) ? $params['id'] : '';
            $target = isset($params['target']) ? $params['target'] : '';
            $link = (isset($params['siteUrl']) && $params['siteUrl'] == 'yes') ? $appSettings->get('webcreator', 'siteurl').$params['href'] : $params['href'];

            $html = '<a href="'.$link.'" target="'.$target.'" class="'.$class.'" id="'.$id.'">'.$shorts[2][$x].'</a>';  

            $content = str_replace($shorts[0][$x], $html, $content);
        }

        return $content;
    }
}