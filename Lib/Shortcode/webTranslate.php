<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webTranslate
 * Find and replaces the string passed with the correct word or phrase, depending on the language of the web or the specified language.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webTranslate extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webTranslate(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);

            if (isset($params['string'])) {
                $string = static::toolBox()->i18n()->trans($params['string']);
            
                if (!empty($params['lang'])) {
                    $string = static::toolBox()->i18n()->customTrans($params['lang'], $params['string']);
                }
    
                $content = str_replace($shorts[0][$x], $string, $content);
            }
        }

        return $content;
    }
}