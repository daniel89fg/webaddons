<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;
use FacturaScripts\Core\Base\DataBase;

/**
 * Shortcode of webSql
 * Obtain any information from any table that we request in the parameters
 *
 * @author Athos Online <info@athosonline.com>
 */
class webSql extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webSql(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $table = $params['table'];
            $fieldname = $params['fieldname'];

            unset($params['table']);
            unset($params['fieldname']);

            $sql = 'SELECT '. $fieldname .' FROM ' . $table . ' WHERE ';
            $cont = 0;
            foreach ($params as $key => $value) {
                $sql .= $key . '="' . $value . '"';
                $cont++;

                if (count($params) > $cont) {
                    $sql .= ' AND ';
                }
            }
            
            $db = new DataBase();
            $result = $db->selectLimit($sql, 1, 0);
            
            if ($result) {
                $content = str_replace($shorts[0][$x], $result[0][$fieldname], $content);
            }
        }
        
        return $content;
    }
}