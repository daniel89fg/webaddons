<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Model\Cliente;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webClient
 * Displays the desired customer information
 *
 * @author Athos Online <info@athosonline.com>
 */
class webClient extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webClient(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $where = [];
            foreach ($params as $key => $value) {
                if ($key != 'fieldname') {
                    $where[] = new DataBaseWhere($key, $value);
                }
            }

            $fieldname = '';

            if ($where) {
                $client = new Cliente();
                $client->loadFromCode('', $where);

                if ($client) {
                    $fieldname = $client->{$params['fieldname']};
                }
            }

            $content = str_replace($shorts[0][$x], $fieldname, $content);
        }
        
        return $content;
    }
}