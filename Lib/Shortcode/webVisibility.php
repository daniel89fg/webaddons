<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Model\Contacto;
use FacturaScripts\Dinamic\Model\User;
use FacturaScripts\Dinamic\Model\Cliente;
use FacturaScripts\Dinamic\Model\GrupoClientes;
use FacturaScripts\Dinamic\Model\RoleUser;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webVisibility
 * Allows you to show content only to the desired users or user groups
 *
 * @author Athos Online <info@athosonline.com>
 */
class webVisibility extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webVisibility(.*?)\][\r\n|\n]*(.*?)[\r\n|\n]*\[\/webVisibility\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }

        $usuario = '';
        if (!empty($_COOKIE['fsIdcontacto'])) {
            $contact = new Contacto();
            $contact->loadFromCode($_COOKIE['fsIdcontacto']);
            $usuario = $contact;
        } else if (!empty($_COOKIE['fsNick'])) {
            $user = new User();
            $user->loadFromCode($_COOKIE['fsNick']);
            $usuario = $user;
        }
        
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $displayGroup = isset($params['displayGroup']) ? explode('|', strtolower($params['displayGroup'])) : null;
            $displayUser = isset($params['displayUser']) ? explode('|', strtolower($params['displayUser'])) : null;
            
            $html = '';

            if (!is_null($displayGroup)) {
                $html = static::displayGroup($usuario, $shorts[2][$x], $displayGroup);
            }
            
            if (!is_null($displayUser)) {
                $html = static::displayUser($usuario, $shorts[2][$x], $displayUser);
            }

            $content = str_replace($shorts[0][$x], $html, $content);
        }
        
        return $content;
    }

    private static function displayUser($usuario, $html, $displayUser)
    {
        if (array_search('all', $displayUser) !== false) {
            return $html;
        }

        if (!empty($usuario)) {
            if (isset($usuario->nick)) {
                if (array_search($usuario->nick, $displayUser) !== false) {
                    return $html;
                }
            } else if (isset($usuario->idcontacto)) {
                if (array_search($usuario->idcontacto, $displayUser) !== false) {
                    return $html;
                }
            }
        }
    }

    private static function displayGroup($usuario, $html, $displayGroup)
    {
        if (array_search('all', $displayGroup) !== false) {
            return $html;
        }

        if (array_search('login', $displayGroup) !== false && !empty($usuario)) {
            return $html;
        }

        if (array_search('guest', $displayGroup) !== false && empty($usuario)) {
            return $html;
        }

        if (!empty($usuario)) {
            if (isset($usuario->nick)) {
                $where = [new DataBaseWhere('nick', $usuario->nick)];
                $role = new RoleUser();
                $role->loadFromCode('', $where);
    
                if (array_search($role->codrole, $displayGroup) !== false) {
                    return $html;
                }
            } else if (isset($usuario->codcliente)) {
                $customer = new Cliente();
                $customer->loadFromCode($usuario->codcliente);

                if (isset($customer->codgrupo)) {
                    $grupo = new GrupoClientes();
                    $grupo->loadFromCode($customer->codgrupo);

                    if (array_search($grupo->nombre, $displayGroup) !== false) {
                        return $html;
                    }
                }
            }
        }
    }
}