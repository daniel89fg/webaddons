<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Model\Contacto;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webContact
 * Displays the desired contact information
 *
 * @author Athos Online <info@athosonline.com>
 */
class webContact extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webContact(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            if (!isset($params['idcontacto']) && !empty($_COOKIE['fsIdcontacto'])) {
                $params['idcontacto'] = $_COOKIE['fsIdcontacto'];
            }

            $where = [];
            foreach ($params as $key => $value) {
                if ($key != 'fieldname') {
                    $where[] = new DataBaseWhere($key, $value);
                }
            }

            $fieldname = '';

            if ($where) {
                $contact = new Contacto();
                $contact->loadFromCode('', $where);

                if ($contact) {
                    $fieldname = $contact->{$params['fieldname']};
                }
            }

            $content = str_replace($shorts[0][$x], $fieldname, $content);
        }
        
        return $content;
    }
}