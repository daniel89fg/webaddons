<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Model\AttachedFile;
use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of webImage
 * Replace the code with an image from the library or a url.
 *
 * @author Athos Online <info@athosonline.com>
 */
class webImage extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webImage(.*?)\]/");

        if (count($shorts[0]) <= 0) {
            return $content;
        }

        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);

            $class = isset($params['class']) ? $params['class'] : '';
            $id = isset($params['id']) ? $params['id'] : '';
            $width = isset($params['width']) ? $params['width'] : '';
            $height = isset($params['height']) ? $params['height'] : '';
            $title = isset($params['title']) ? $params['title'] : '';
            $alt = isset($params['alt']) ? $params['alt'] : '';
            $onlyUrl = (isset($params['onlyUrl']) && $params['onlyUrl'] == 'yes') ? true : false;

            if (isset($params['idfile'])) {
                $file = new AttachedFile();
                $file->loadFromCode($params['idfile']);
                $url = $file->url('download-permanent');
            } else if (isset($params['src'])) {
                $url = $params['src'];
            }

            if ($onlyUrl) {
                $content = str_replace($shorts[0][$x], $url, $content);
            } else {
                $img = '<img 
                    src="'.$url.'"
                    class="'.$class.'"
                    id="'.$id.'"
                    title="'.$title.'"
                    alt="'.$alt.'"
                    width="'.$width.'"
                    height="'.$height.'"
                >';

                $content = str_replace($shorts[0][$x], $img, $content);
            }
        }

        return $content;
    }
}