<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebAddons\Lib\Shortcode;

use FacturaScripts\Dinamic\Model\User;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;

/**
 * Shortcode of WebUser
 * Displays the desired user information
 *
 * @author Athos Online <info@athosonline.com>
 */
class webUser extends Shortcode
{
    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webUser(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            if (!isset($params['nick']) && !empty($_COOKIE['fsNick'])) {
                $params['nick'] = $_COOKIE['fsNick'];
            }

            $where = [];
            foreach ($params as $key => $value) {
                if ($key != 'fieldname') {
                    $where[] = new DataBaseWhere($key, $value);
                }
            }

            $fieldname = '';

            if ($where) {
                $contact = new User();
                $contact->loadFromCode('', $where);

                if ($contact) {
                    $fieldname = $contact->{$params['fieldname']};
                }
            }

            $content = str_replace($shorts[0][$x], $fieldname, $content);
        }
        
        return $content;
    }
}