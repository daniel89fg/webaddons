<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebShortcodes;

use FacturaScripts\Core\Base\InitClass;
use FacturaScripts\Plugins\WebCreator\Lib\Shortcode\Shortcode;

/**
 * Description of Init
 *
 * @author Athos Online <info@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        Shortcode::addCode('image', 'webImage');
        Shortcode::addCode('datehour', 'webDateHour');
        Shortcode::addCode('link', 'webLink');
        Shortcode::addCode('button', 'webButton');
        Shortcode::addCode('visibility', 'webVisibility');
        Shortcode::addCode('client', 'webClient');
        Shortcode::addCode('contact', 'webContact');
        Shortcode::addCode('user', 'webUser');
        Shortcode::addCode('sql', 'webSql');
        Shortcode::addCode('translate', 'webTranslate');
    }

    public function update()
    {
        ;
    }
}